import { Scope } from '@babel/traverse';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Icon from 'react-native-vector-icons/AntDesign';

import NetInfo from '@react-native-community/netinfo';

import axios from 'axios';
import Movie from './Movie';
import { parse } from '@babel/core';

export default function Home ({navigation}){
    const[moviesList, setMovies] = useState([]);
    const[done, setDone] = useState(false);
    const[failLoad, setFailLoad] = useState(false);
    const[connection, setConnection] = useState(false);

    useEffect(() => {
        NetInfo.fetch().then(state=>{
            if(state.isConnected == true){
                setConnection(true);
                console.log('Connection succes!');
            }else{
                setConnection(false);
                console.log('Connection failed!');
            }
        });
        axios.get('http://code.aldipee.com/api/v1/movies').then(responseData => {
            let movies = responseData.data.results;
            setMovies(movies);
            console.log(movies);
            setDone(true);
        }).catch(err=>{
            console.log("ERR", err);
            setFailLoad(true);
        });
    }, []);

    return(
        <>
            {!done? 
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large" color="#ffffff" style={styles.loadingStyle}/>
                </View>
                :
                (failLoad == true? 
                <View style={styles.loadingContainer}>
                    <Text style={{color : 'white', fontSize : 20}}>Failed to load data!</Text>
                  </View>
                    :    
                    <View style={styles.mainContainer}>
                        <Text style={styles.appTitle}>
                            {connection == true ? 'MovieMAX' : 'Connection Failed'}
                        </Text>
                        <View style={styles.recomContainer}>
                            <Text style={styles.textRecom}>Recommended</Text>
                            <View style={styles.recomendHighlight}>
                               <ScrollView horizontal={true}>
                                    {
                                         moviesList.map(item=>{
                                             return(
                                                 <TouchableOpacity key={item.id} onPress={()=> navigation.navigate('Movie', {paramKey : item.id})}>
                                                     <Image style={styles.posterStyle} source={{uri: item.poster_path}} key={item.id}/>
                                                 </TouchableOpacity>
                                             );
                                         })
                                     }
                               </ScrollView>
                            </View>
                        </View>
                        <View style={styles.lastestContainer}>
                            <Text style={styles.lastText}>Lastest Upload</Text>
                            <View>
                                {
                                    moviesList.slice(0,3).map(item=>{

                                        return(
                                            <View style={styles.lastUpItem} key={item.id}>
                                                <Image style={styles.lastUpImage} source={{uri : item.poster_path}}/>
                                                <View style={styles.lastUpDesc}>
                                                    <Text style={styles.lastUpTitle}>{item.original_title}</Text>
                                                    <Text style={styles.lastUpText}>Release: {item.release_date}</Text>
                                                    <Text style={styles.lastUpText}>
                                                        <Icon name="star" size={15} color="yellow"/>
                                                        {'\t' + item.vote_average}
                                                    </Text>
                                                    {item.adult == false ? <Text style={styles.lastUpText}>For all ages</Text> : <Text style={styles.lastUpText}>Only for adult</Text>}
                                                    <TouchableOpacity style={styles.buttonDetail} onPress={()=> navigation.navigate('Movie', {paramKey : item.id})}>
                                                        <Text style={styles.showMoreText}>Show More</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        );
                                    })
                                }
                            </View>
                        </View>
                    </View>    
                )
            }
        </>
    );    
}

const styles = StyleSheet.create({
    mainContainer : {
        height : '100%',
        width : '100%',
        backgroundColor : 'black',
        paddingTop : 20
    },
    appTitle : {
        color : 'white',
        justifyContent : 'center',
        alignItems : 'center',
        textAlign : 'center',
        fontSize : 15,
    },
    textRecom : {
        color: 'white',
        fontFamily : 'Rubik',
        fontStyle : 'normal',
        fontSize : 15,
        lineHeight : 19
    },
    recomContainer : {
        marginLeft : 13,
        paddingRight : 13,
    },
    recomendHighlight : {
        flexDirection : 'row'
    },  
    posterStyle : {
        height : 155,
        width : 100,
        marginRight : 20,
        marginTop : 10
    },
    lastestContainer : {
        marginTop : 20,
        marginLeft : 13,
    },
    lastText : {
        color: 'white',
        fontFamily : 'Rubik',
        fontStyle : 'normal',
        fontSize : 15,
        lineHeight : 19,
        marginBottom : 10,
    },
    lastUpImage :{
        height : 110,
        width : 110,
    },
    lastUpItem : {
        flexDirection : 'row',
        marginBottom : 10,
    },
    lastUpDesc : {
        marginLeft : 20,
    },
    lastUpTitle : {
        color : 'white'
    },
    lastUpText : {
        color : 'white'
    },
    buttonDetail : {
        backgroundColor : 'green',
        width : 100,
        height : 30,
        marginTop : 10,
        justifyContent : 'center',
        alignItems : 'center'
    },
    showMoreText : {
        color : 'white'
    },
    loadingContainer : {
        backgroundColor: 'black',
        width : '100%',
        height : '100%',
        alignItems : 'center',
        justifyContent : 'center'
    },  
    loadingScreen : {
      justifyContent : 'center'
    },
});