import React, {useState, useEffect, useCallback} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  Share,
  RefreshControl,
  ActivityIndicator
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import axios from 'axios';  

import Icon from 'react-native-vector-icons/Fontisto';
import Icon2 from 'react-native-vector-icons/Entypo';
import Star from 'react-native-vector-icons/AntDesign';
import ReactLoading from 'react-loading';

export default function Movie ({route, navigation}){
  const[moviesList, setMovies] = useState([]);
  const[selectMovies, setSelectMovies] = useState([]);
  const[artisInMovie, setArtis] = useState([]);
  const[refreshing, setRefreshing] = useState(false);
  const[done, setDone] = useState(false);
  const[failLoad, setFailLoad] = useState(false);

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }

  const onRefresh = useCallback(()=>{
    setRefreshing(true);
    wait(2000).then(()=>setRefreshing(false));
  }, []);

    useEffect(() => {
       setTimeout(()=>{
        // setDone(false);
        axios.get('http://code.aldipee.com/api/v1/movies/' + route.params.paramKey).then(responseData => {
          let movies = responseData.data;
          let select = responseData.data.genres;
          let listArtis = responseData.data.credits.cast;
          setMovies(movies);
          setSelectMovies(select);
          setArtis(listArtis);
          setDone(true);
          console.log(movies);
      }).catch(err=>{
          console.log("ERR", err);
          setFailLoad(true);
      });
       }, 2000)
    }, []);

    const onShare = async () => {
      try {
        const result = await Share.share({
          message:
            'React Native | A framework for building native apps using React',
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    };

    return(
      <>
        {done == false? 
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color="#ffffff" style={styles.loadingStyle}/>
          </View>
          : 
          (failLoad == true? 
            <View style={styles.loadingContainer}>
              <Text style={{color : 'white', fontSize : 20}}>Failed to load data!</Text>
            </View>
            :
            <View style={styles.mainContainer}>
          <ImageBackground source={{uri: moviesList.backdrop_path}} resizeMode={'cover'} style={styles.backgroundPict}>
                  <View style={styles.topNav}>
                    <TouchableOpacity style={styles.backButton} onPress={()=> navigation.navigate('Home')}>
                      <Icon2 name="back" size={30} color={'white'}/>
                    </TouchableOpacity>
                    <View style={styles.shareLikeContainer}>
                        <TouchableOpacity style={styles.likeButton}>
                          <Icon2 name="heart" size={35} color={'white'} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.shareButton} onPress={onShare}>
                          <Icon name="share-a" size={25} color={'white'}/>
                        </TouchableOpacity>
                      </View>
                  </View>
          </ImageBackground>
          <View style={styles.detailsShortInfo}>
            <Image source={{uri:moviesList.poster_path}} style={styles.posterStyle}/>
            <View style={styles.infoShort}>
              <Text style={styles.infoShortText}>{moviesList.title}</Text>
              <Text style={styles.infoShortText}>Release : {moviesList.release_date}</Text>
              <Text style={styles.infoShortText}><Star name="star" style={{color : 'yellow'}} size={20}/>{moviesList.vote_average}</Text>
              <Text style={styles.infoShortText}>Runtime : {moviesList.runtime}</Text>
              <Text style={styles.infoShortText}>{moviesList.tagline}</Text>
              <Text style={styles.infoShortText}>{moviesList.status}</Text>
            </View>
          </View>
          <View style={styles.genreContainer}>
            <Text style={{color : 'white', fontSize : 15, fontWeight : 'bold'}}>GENRE : </Text>
            <View style={styles.genreList}>
              {
                selectMovies.map(item => {
                  return(
                    <Text key={item.id} style={{color: 'white', marginRight : 20}}>{item.name}</Text>
                  );
                })
              }
            </View>
          </View>
          <ScrollView refreshControl={
            <RefreshControl 
              refreshing={refreshing}
              onRefresh={onRefresh}
            />
          }>
          <View style={styles.synopshisContainer}>
              <Text style={{color : 'white', fontSize : 15, fontWeight : 'bold'}}>SYNOPSHIS : </Text>
              <Text style={{color: 'white'}}>{moviesList.overview}</Text>
          </View>
          <View style={styles.ActorArtisContainer}>
              <Text style={{color : 'white', fontSize : 15, fontWeight : 'bold'}}>Actor / Artis</Text>
                <View style={styles.artisInfoContainer}>
                  {
                    artisInMovie.map(item => {
                      return(
                        <View style={styles.artisContainer} key={item.id}>
                          <Image source={{uri : item.profile_path}} style={styles.profileArtis}/>
                          <Text style={styles.artisName}>{item.name.slice(0, 10)}</Text>
                        </View>
                      );
                    })
                  }
                </View>
          </View>
          </ScrollView>
        </View>
          
          )
        }
      </>
    );    
}

const styles = StyleSheet.create({
  mainContainer : {
    width : '100%',
    height : '100%',
    backgroundColor : 'black',
  },
  loadingContainer : {
    backgroundColor: 'black',
    width : '100%',
    height : '100%',
    alignItems : 'center',
    justifyContent : 'center'
  },  
  loadingScreen : {
    justifyContent : 'center'
  },
  backgroundPict : {
    width : '100%',
    height : '55%',
  },
  topNav : {
    paddingTop : 10,
    paddingLeft : 8,
    paddingRight : 8,
    flexDirection : 'row',
    justifyContent : 'space-between',
  },
  shareLikeContainer : {
    flexDirection : 'row'
  },
  shareButton : {
    marginLeft : 10,
  },
  detailsShortInfo : {
    marginLeft: '4%',
    marginRight: '4%',
    flexDirection : 'row',
    marginTop : -230,
    backgroundColor : 'white',
    width : '92%',
    height : 180,
    paddingLeft : 9,
    paddingTop : 12,
    paddingBottom : 12
  },
  posterStyle : {
    width : 120,
    height : 150,
  },
  infoShort : {
    marginLeft : 30
  },
  genreContainer : {
    paddingLeft : 13,
    marginTop : 20,
    paddingBottom : 10
  },
  genreList : {
    marginTop : 5,
    flexDirection : 'row'
  },
  synopshisContainer : {
    paddingLeft : 13,
    paddingRight : 13,
    marginTop : 10,
  },
  infoShort : {
   paddingLeft : 13 
  },
  infoShortText : {
    color : 'black',
    fontSize : 15,
  },
  ActorArtisContainer: {
    paddingLeft : 13,
    paddingRight: 10,
    paddingTop: 20,
  },
  artisInfoContainer : {
    flexDirection : 'row',
    flexWrap : 'wrap',
    justifyContent : 'space-between',
    alignItems : 'center',
    paddingBottom : 20,
    marginTop : 10,
    height : '100%',
  },
  artisContainer : {
    flexDirection : 'column',
    justifyContent : 'center',
    alignItems : 'center',
    marginBottom : 20
  },
  profileArtis : {
    width : 100,
    height : 100
  },
  artisName : {
    color : 'white'
  },
});