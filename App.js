
import React, {useEffect}from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SplashScreen from 'react-native-splash-screen';
import Icon from 'react-native-vector-icons';

import Home from './screen/Home';
import Movie from './screen/Movie';
import Spinner from 'react-native-loading-spinner-overlay';

const Stack = createNativeStackNavigator();

function App(props){
  useEffect(()=>{
    SplashScreen.hide();
  });

  return (
   <NavigationContainer>
     <Stack.Navigator initialRouteName='Home'>
        <Stack.Screen name='Home'component={Home} options={{headerShown: false}}/>
        <Stack.Screen name='Movie' component={Movie} options={{headerShown: false}}/>
     </Stack.Navigator>
   </NavigationContainer>
  );
};

const styles = StyleSheet.create({

});

export default App;
